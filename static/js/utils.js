/**
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
**/

export function format_docs(relation, alvisir, rtype) {

  let docs = "";
  if ( relation[0].includes(', ') ) { docs = relation[0].split(', '); }
  else                              { docs = relation[0].split(','); }
  let docids = "";
  for ( let i = 0; i < docs.length ; i++ ) {
    if ( relation[5] == 'PubMed') {
      let taxon = relation[1].split(', ')[0].replace(/\s/g, "+").replace("=", "\\=").replace("~","\\~").replace("(","\\(").replace(")","\\)").replace("[","\\[").replace("]","\\]").replace(/\+(and|or|not)\+/, "\+\\$1\+");
      let element = relation[3].split(', ')[0].replace(/\s/g, "+").replace("=", "\\=").replace("~","\\~").replace("(","\\(").replace(")","\\)").replace("[","\\[").replace("]","\\]").replace(/\+(and|or|not)\+/, "\+\\$1\+");
      let path = relation[6].split(',')[0];
      let rel_type = '';
      if ( rtype == 'habitat' )         { rel_type = 'lives+in'; }
      else if ( rtype == 'phenotype' )  { rel_type = 'exhibits'; }
      else if ( rtype == 'use' )        { rel_type = 'studied+for'; }
      docids += "<a target='_blank' class='doc' href='"+alvisir+"search?q=%22"+taxon+"%22+"+rel_type+"+%7B"+rtype+"%7D"+path+"/+%22"+element+"%22+pmid="+docs[i]+"'>"+docs[i]+"</a>, ";
    }
    else if ( relation[5] == 'GenBank' ) {
      docids += "<a target='_blank' class='doc' href='https://www.ncbi.nlm.nih.gov/nuccore/"+docs[i]+"'>"+docs[i]+"</a>, ";
    }
    else if ( relation[5] == 'DSMZ' ) {
      docids += "<a target='_blank' class='doc' href='https://bacdive.dsmz.de/strain/"+docs[i]+"'>"+docs[i]+"</a>, ";
    }
    else if ( relation[5] == 'CIRM-BIA' && relation[10] != 'Restricted' ) {
      let cirmid = docs[i].split(', ')[0].replace(/.*\sCIRM-BIA\s/g, "");
      if ( cirmid != '-' ) {
        docids += "<a target='_blank' class='doc' href='https://collection-cirmbia.fr/page/Display_souches/"+cirmid+"'>"+cirmid+"</a>, ";
      }
    }
    else {
      docids = relation[0] + ", "
    }
    // CIRM-Levures
    // CIRM-CBPF
  }
  docids = docids.slice(0, -2);

  return docids;
}

export function createTable(path, rel_type, tablename) {
  let l_path = path.split('/');
  let taxonid = l_path[l_path.length-1];

        $.getJSON($SCRIPT_ROOT + '/_get_list_relations',
          { taxonid: taxonid,
            type: rel_type
          },
          function success(relations) {
            $('#hide').css( 'display', 'block' );
            $('#'+tablename).DataTable().destroy();
            $('#'+tablename).DataTable(
              {
               dom: 'lifrtBp', // 'Bfrtip'
               data: relations,
               buttons: [
                          {
                              extend: 'copyHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                var table_id = node.getAttribute('aria-controls');
                                                if ( idx == 0 ) { return false; } // Never Source Text
                                                else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'csvHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                var table_id = node.getAttribute('aria-controls');
                                                if ( idx == 0 ) { return false; } // Never Source Text
                                                else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'excelHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                 var table_id = node.getAttribute('aria-controls');
                                                 if ( idx == 0 ) { return false; } // Never Source Text
                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                 else { return $('#' + table_id).DataTable().column( idx ).visible(); } }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'pdfHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                 var table_id = node.getAttribute('aria-controls');
                                                 if ( idx == 0 ) { return false; } // Never Source Text
                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                 return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          'colvis'
                      ],
               columns: [
                 // Source text
                 {"render": function(data, type, row, meta) {
                    let rtype = '';
                    if ( row[2] == 'Have produced tmp 2' ) { rtype = 'Oligosaccharide_type'; }
                    else if ( row[2] == 'Have produced tmp 1' ) { rtype = 'Oligosaccharide_name'; }
                    else if ( row[2] == 'Have produced 2' ) { rtype = 'lactationstage'; }
                    else if ( row[2] == 'Have produced 1' ) { rtype = 'sampletype'; }
                    else if ( row[2] == 'Has given birth since' ) { rtype = 'postpartumage'; }
                    else if ( row[2] == 'Has a physiological stage' ) { rtype = 'physiologicalstage'; }
                    else if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
                    else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'use'; }
                    else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
                    var docids = format_docs(row, alvisir, rtype);
                    let docs = "";
                    if ( data.includes(', ') ) { docs = data.split(', '); }
                    else                       { docs = data.split(','); }
                    let docs_f = "";
                    if ( docs.length > 2 ) { // 3
                      docs_f = docids.split(", ").slice(0,2).join(', ') + ", ..."; // 0,3
                    }
                    else {
                      docs_f = docids;
                    }
                    return docs_f;
                  }},
                 // Scientific name of taxon
                 {"render": function ( data, type, row, meta ) {
                    // let taxa = row[1].split(', ');
                    // let taxon = taxa[0];
                    let taxa = row[12];
                    if ( row[9].includes("ncbi") ) {
                      // taxon = "<a target='_blank' class='doc' href='https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id="+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
                      taxon = "<a target='_blank' class='doc' href='https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id="+row[9].replace(/.+:/ig, '')+"'>"+taxa+"</a>";
                    }
                    else if ( row[9].includes("bd") ) {
                      // taxon = "<a target='_blank' class='doc' href='https://bacdive.dsmz.de/strain/"+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
                      taxon = "<a target='_blank' class='doc' href='https://bacdive.dsmz.de/strain/"+row[9].replace(/.+:/ig, '')+"'>"+taxa+"</a>";
                    }
                    return taxon;
                  }},
                 // Relation type
                 {"render": function (data, type, row, meta) {
                     return row[2];
                 }},
                 // Reference class
                 {"render": function (data, type, row, meta) {
                     //return row[3].split(',')[0];
                     return row[11];
                 }},
                 // QPS
                 {"orderable": false, "render": function (data, type, row, meta) {
                     return row[4];
                 }},
                 // Source
                 {"render": function (data, type, row, meta) {
                     return row[5];
                 }},
                 // Form taxon
                 {"render": function (data, type, row, meta) {
                    let taxs = row[1].split(', ');
                    let forms = "";
                    let i = 1;
                    if ( rel_type == 'have_produced_tmp_2' || rel_type == 'have_produced_tmp_1' || rel_type == 'have_produced_2' || rel_type == 'have_produced_1' || rel_type == 'has_a_physiological_stage' || rel_type == 'has_given_birth_since' ) { i = 0; }
                    for ( i ; i < taxs.length ; i++ ) { //let i = 1;
                      forms += taxs[i]
                      if ( i != taxs.length - 1 ) { forms += ", " }
                    }
                    return forms;
                 }},
                 // Form reference
                 {"render": function (data, type, row, meta) {
                     let elts = row[3].split(', ');
                     let forms = "";
                     let i = 1;
                     if ( rel_type == 'have_produced_tmp_2' || rel_type == 'have_produced_tmp_1' || rel_type == 'have_produced_2' || rel_type == 'have_produced_1' || rel_type == 'has_a_physiological_stage' || rel_type == 'has_given_birth_since' ) { i = 0; }
                     for ( i ; i < elts.length ; i++ ) { //let i = 1;
                       forms += elts[i]
                       if ( i != elts.length - 1 ) { forms += ", " }
                     }
                     return forms;
                 }},
                 // Full source text
                 {"render": function (data, type, row, meta) {
                    let rtype = '';
                    if ( row[2] == 'Have produced tmp 2' ) { rtype = 'Oligosaccharide_type'; }
                    else if ( row[2] == 'Have produced tmp 1' ) { rtype = 'Oligosaccharide_name'; }
                    else if ( row[2] == 'Have produced 2' ) { rtype = 'lactationstage'; }
                    else if ( row[2] == 'Have produced 1' ) { rtype = 'sampletype'; }
                    else if ( row[2] == 'Has given birth since' ) { rtype = 'postpartumage'; }
                    else if ( row[2] == 'Has a physiological stage' ) { rtype = 'physiologicalstage'; }
                    else if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
                    else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'use'; }
                    else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
                    var docids = format_docs(row, alvisir, rtype);
                    return docids;
                 }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[9];
                   }},
                 {"visible": false, "render": function (data, type, row, meta) {
                    return row[7];
                  }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[8];
                 }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[6];
                 }}
               ]
              });
        });
}

export function createTableRev(path, rel_type, tablename) {

        let l_path = path.split('/');
        let ontobiotopeid = l_path[l_path.length-1];

        $.getJSON($SCRIPT_ROOT + '/_get_list_relations',
          {
            ontobiotopeid: ontobiotopeid,
            type: rel_type
          },
          function success(relations) {
            $('#hide').css( 'display', 'block' );
            $('#'+tablename).DataTable().destroy();
            $('#'+tablename).DataTable(
              {dom: 'lifrtBp', // 'Bfrtip'
               data: relations,
               buttons: [
                          {
                              extend: 'copyHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                var table_id = node.getAttribute('aria-controls');
                                                if ( idx == 0 ) { return false; } // Never Source Text
                                                else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'csvHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                var table_id = node.getAttribute('aria-controls');
                                                if ( idx == 0 ) { return false; } // Never Source Text
                                                else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'excelHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                 var table_id = node.getAttribute('aria-controls');
                                                 if ( idx == 0 ) { return false; } // Never Source Text
                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                 else { return $('#' + table_id).DataTable().column( idx ).visible(); } }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'pdfHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                 var table_id = node.getAttribute('aria-controls');
                                                 if ( idx == 0 ) { return false; } // Never Source Text
                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                 return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          'colvis'
                      ],
               columns: [
                 {"render": function(data, type, row, meta) {
                    let rtype = '';
                    if ( row[2] == 'Have produced tmp 2' ) { rtype = 'Oligosaccharide_type'; }
                    else if ( row[2] == 'Have produced tmp 1' ) { rtype = 'Oligosaccharide_name'; }
                    else if ( row[2] == 'Have produced 2' ) { rtype = 'lactationstage'; }
                    else if ( row[2] == 'Have produced 1' ) { rtype = 'sampletype'; }
                    else if ( row[2] == 'Has given birth since' ) { rtype = 'postpartumage'; }
                    else if ( row[2] == 'Has a physiological stage' ) { rtype = 'physiologicalstage'; }
                    else if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
                    else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'use'; }
                    else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
                    var docids = format_docs(row, alvisir, rtype);
                    let docs = "";
                    if ( data.includes(', ') ) { docs = data.split(', '); }
                    else                       { docs = data.split(','); }
                    let docs_f = "";
                    if ( docs.length > 2 ) { // 3
                      docs_f = docids.split(", ").slice(0,2).join(', ') + ", ..."; // 0,3
                    }
                    else {
                      docs_f = docids;
                    }
                    return docs_f;
                  }},
                 {"render": function (data, type, row, meta) {
                    //  return row[3].split(',')[0];
                     return row[11];
                 }},
                 {"render": function (data, type, row, meta) {
                     return row[2];
                 }},
                 {"render": function ( data, type, row, meta ) {
                  //  let taxa = row[1].split(', ');
                  //  let taxon = taxa[0];
                   let taxa = row[12];
                   if ( row[9].includes("ncbi") ) {
                    //  taxon = "<a target='_blank' class='doc' href='https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id="+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
                     taxon = "<a target='_blank' class='doc' href='https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id="+row[9].replace(/.+:/ig, '')+"'>"+taxa+"</a>";
                   }
                   else if ( row[9].includes("bd") ) {
                    //  taxon = "<a target='_blank' class='doc' href='https://bacdive.dsmz.de/strain/"+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
                     taxon = "<a target='_blank' class='doc' href='https://bacdive.dsmz.de/strain/"+row[9].replace(/.+:/ig, '')+"'>"+taxa+"</a>";
                   }
                   return taxon;
                  }},
                 {"orderable": false, "render": function (data, type, row, meta) {
                     return row[4];
                 }},
                 {"render": function (data, type, row, meta) {
                     return row[5];
                 }},
                 {"render": function (data, type, row, meta) {
                     let elts = row[3].split(', ');
                     let forms = "";
                     let i = 1;
                     if ( rel_type == 'have_produced_tmp_2' || rel_type == 'have_produced_tmp_1' || rel_type == 'have_produced_2' || rel_type == 'have_produced_1' || rel_type == 'has_a_physiological_stage' || rel_type == 'has_given_birth_since' ) { i = 0; }
                     for ( i ; i < elts.length ; i++ ) { //let i = 1;
                       forms += elts[i]
                       if ( i != elts.length - 1 ) { forms += ", " }
                     }
                     return forms;
                 }},
                 {"render": function (data, type, row, meta) {
                   let taxs = row[1].split(', ');
                   let forms = "";
                   let i = 1;
                   if ( rel_type == 'have_produced_tmp_2' || rel_type == 'have_produced_tmp_1' || rel_type == 'have_produced_2' || rel_type == 'have_produced_1' || rel_type == 'has_a_physiological_stage' || rel_type == 'has_given_birth_since' ) { i = 0; }
                   for ( i ; i < taxs.length ; i++ ) { //let i = 1;
                     forms += taxs[i]
                     if ( i != taxs.length - 1 ) { forms += ", " }
                   }
                   return forms;
                 }},
                 {"render": function (data, type, row, meta) {
                    let rtype = '';
                    if ( row[2] == 'Have produced tmp 2' ) { rtype = 'Oligosaccharide_type'; }
                    else if ( row[2] == 'Have produced tmp 1' ) { rtype = 'Oligosaccharide_name'; }
                    else if ( row[2] == 'Have produced 2' ) { rtype = 'lactationstage'; }
                    else if ( row[2] == 'Have produced 1' ) { rtype = 'sampletype'; }
                    else if ( row[2] == 'Has given birth since' ) { rtype = 'postpartumage'; }
                    else if ( row[2] == 'Has a physiological stage' ) { rtype = 'physiologicalstage'; }
                    else if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
                    else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'use'; }
                    else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
                    var docids = format_docs(row, alvisir, rtype);
                    return docids;
                 }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[8];
                 }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[6];
                 }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[9];
                   }},
                 {"visible": false, "render": function (data, type, row, meta) {
                    return row[7];
                  }}
               ]
              });
        });
}

export function createTableWithArg(taxonid, etype, tablename) {

    $.getJSON($SCRIPT_ROOT + '/_get_list_relations',
          { taxonid: taxonid,
            type: etype//'habitat'
          },
          function success(relations) {
            $('#hide').css( 'display', 'block' );
            $('#'+tablename).DataTable().destroy();
            $('#'+tablename).DataTable(
              {
               dom: 'lifrtBp', // 'Bfrtip'
               data: relations,
               buttons: [
                          {
                              extend: 'copyHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                var table_id = node.getAttribute('aria-controls');
                                                if ( idx == 0 ) { return false; } // Never Source Text
                                                else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'csvHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                var table_id = node.getAttribute('aria-controls');
                                                if ( idx == 0 ) { return false; } // Never Source Text
                                                else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'excelHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                 var table_id = node.getAttribute('aria-controls');
                                                 if ( idx == 0 ) { return false; } // Never Source Text
                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                 else { return $('#' + table_id).DataTable().column( idx ).visible(); } }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'pdfHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                 var table_id = node.getAttribute('aria-controls');
                                                 if ( idx == 0 ) { return false; } // Never Source Text
                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                 return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          'colvis'
                      ],
               columns: [
                 {"render": function(data, type, row, meta) {
                    let rtype = '';
                    if ( row[2] == 'Have produced tmp 2' ) { rtype = 'Oligosaccharide_type'; }
                    else if ( row[2] == 'Have produced tmp 1' ) { rtype = 'Oligosaccharide_name'; }
                    else if ( row[2] == 'Have produced 2' ) { rtype = 'lactationstage'; }
                    else if ( row[2] == 'Have produced 1' ) { rtype = 'sampletype'; }
                    else if ( row[2] == 'Has given birth since' ) { rtype = 'postpartumage'; }
                    else if ( row[2] == 'Has a physiological stage' ) { rtype = 'physiologicalstage'; }
                    else if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
                    else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'use'; }
                    else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
                    var docids = format_docs(row, alvisir, rtype);
                    let docs = "";
                    if ( data.includes(', ') ) { docs = data.split(', '); }
                    else                       { docs = data.split(','); }
                    let docs_f = "";
                    if ( docs.length > 2 ) { // 3
                      docs_f = docids.split(", ").slice(0,2).join(', ') + ", ..."; // 0,3
                    }
                    else {
                      docs_f = docids;
                    }
                    return docs_f;
                  }},
                 {"render": function ( data, type, row, meta ) {
                    // let taxa = row[1].split(', ');
                    // let taxon = taxa[0];
                    if ( row[9].includes("ncbi") ) {
                      // taxon = "<a target='_blank' class='doc' href='https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id="+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
                      taxon = "<a target='_blank' class='doc' href='https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id="+row[9].replace(/.+:/ig, '')+"'>"+row[12]+"</a>";
                    }
                    else if ( row[9].includes("bd") ) {
                      // taxon = "<a target='_blank' class='doc' href='https://bacdive.dsmz.de/strain/"+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
                      taxon = "<a target='_blank' class='doc' href='https://bacdive.dsmz.de/strain/"+row[9].replace(/.+:/ig, '')+"'>"+row[12]+"</a>";
                    }
                    return taxon;
                  }},
                 {"render": function (data, type, row, meta) {
                     return row[2];
                 }},
                 {"render": function (data, type, row, meta) {
                    //  return row[3].split(',')[0];
                    return row[11];
                 }},
                 {"orderable": false, "render": function (data, type, row, meta) {
                     return row[4];
                 }},
                 {"render": function (data, type, row, meta) {
                     return row[5];
                 }},
                 {"render": function (data, type, row, meta) {
                    let taxs = row[1].split(', ');
                    let forms = "";
                    let i = 1;
                    if ( etype == 'have_produced_tmp_2' || etype == 'have_produced_tmp_1' || etype == 'have_produced_2' || etype == 'have_produced_1' || etype == 'has_a_physiological_stage' || etype == 'has_given_birth_since' ) { i = 0; }
                    for ( i ; i < taxs.length ; i++ ) {
                      forms += taxs[i]
                      if ( i != taxs.length - 1 ) { forms += ", " }
                    }
                    return forms;
                 }},
                 {"render": function (data, type, row, meta) {
                     let elts = row[3].split(', ');
                     let forms = "";
                     let i = 1;
                     if ( etype == 'have_produced_tmp_2' || etype == 'have_produced_tmp_1' || etype == 'have_produced_2' || etype == 'have_produced_1' || etype == 'has_a_physiological_stage' || etype == 'has_given_birth_since' ) { i = 0; }
                     for ( i ; i < elts.length ; i++ ) {
                       forms += elts[i]
                       if ( i != elts.length - 1 ) { forms += ", " }
                     }
                     return forms;
                 }},
                 {"render": function (data, type, row, meta) {
                    let rtype = '';
                    if ( row[2] == 'Have produced tmp 2' ) { rtype = 'Oligosaccharide_type'; }
                    else if ( row[2] == 'Have produced tmp 1' ) { rtype = 'Oligosaccharide_name'; }
                    else if ( row[2] == 'Has product 2' ) { rtype = 'lactationstage'; }
                    else if ( row[2] == 'Has product 1' ) { rtype = 'sampletype'; }
                    else if ( row[2] == 'Has given birth since' ) { rtype = 'postpartumage'; }
                    else if ( row[2] == 'Has a physiological stage' ) { rtype = 'physiologicalstage'; }
                    else if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
                    else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'use'; }
                    else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
                    var docids = format_docs(row, alvisir, rtype);
                    return docids;
                 }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[9];
                   }},
                 {"visible": false, "render": function (data, type, row, meta) {
                    return row[7];
                  }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[8];
                 }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[6];
                 }}
               ]
              });
        });
}

export function createTableWithArgRev(ontobiotopeid, etype, tablename) {

    $.getJSON($SCRIPT_ROOT + '/_get_list_relations',
          { ontobiotopeid: ontobiotopeid,
            type: etype
          },
          function success(relations) {
            $('#hide').css( 'display', 'block' );
            $('#'+tablename).DataTable().destroy();
            $('#'+tablename).DataTable(
              {dom: 'lifrtBp', // 'Bfrtip'
               data: relations,
               buttons: [
                          {
                              extend: 'copyHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                var table_id = node.getAttribute('aria-controls');
                                                if ( idx == 0 ) { return false; } // Never Source Text
                                                else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'csvHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                var table_id = node.getAttribute('aria-controls');
                                                if ( idx == 0 ) { return false; } // Never Source Text
                                                else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'excelHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                 var table_id = node.getAttribute('aria-controls');
                                                 if ( idx == 0 ) { return false; } // Never Source Text
                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                 else { return $('#' + table_id).DataTable().column( idx ).visible(); } }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'pdfHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                 var table_id = node.getAttribute('aria-controls');
                                                 if ( idx == 0 ) { return false; } // Never Source Text
                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                 return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          'colvis'
                      ],
               columns: [
                 {"render": function(data, type, row, meta) {
                    let rtype = '';
                    if ( row[2] == 'Have produced 2' ) { rtype = 'lactationstage'; }
                    else if ( row[2] == 'Have produced 1' ) { rtype = 'sampletype'; }
                    else if ( row[2] == 'Has given birth since' ) { rtype = 'postpartumage'; }
                    else if ( row[2] == 'Has a physiological stage' ) { rtype = 'physiologicalstage'; }
                    else if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
                    else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'use'; }
                    else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
                    var docids = format_docs(row, alvisir, rtype);
                    let docs = "";
                    if ( data.includes(', ') ) { docs = data.split(', '); }
                    else                       { docs = data.split(','); }
                    let docs_f = "";
                    if ( docs.length > 2 ) { // 3
                      docs_f = docids.split(", ").slice(0,2).join(', ') + ", ..."; // 0,3
                    }
                    else {
                      docs_f = docids;
                    }
                    return docs_f;
                  }},
                 {"render": function (data, type, row, meta) {
                     return row[3].split(',')[0];
                 }},
                 {"render": function (data, type, row, meta) {
                     return row[2];
                 }},
                 {"render": function ( data, type, row, meta ) {
                   let taxa = row[1].split(', ');
                   let taxon = taxa[0];
                   if ( row[9].includes("ncbi") ) {
                     taxon = "<a target='_blank' class='doc' href='https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id="+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
                   }
                   else if ( row[9].includes("bd") ) {
                     taxon = "<a target='_blank' class='doc' href='https://bacdive.dsmz.de/strain/"+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
                   }
                   return taxon;
                  }},
                 {"orderable": false, "render": function (data, type, row, meta) {
                     return row[4];
                 }},
                 {"render": function (data, type, row, meta) {
                     return row[5];
                 }},
                 {"render": function (data, type, row, meta) {
                   let elts = row[3].split(', ');
                   let forms = "";
                   let i = 1;
                   if ( etype == 'have_produced_2' || etype == 'have_produced_1' || etype == 'has_a_physiological_stage' || etype == 'has_given_birth_since' ) { i = 0; }
                   for ( let i = 1; i < elts.length ; i++ ) {
                     forms += elts[i]
                     if ( i != elts.length - 1 ) { forms += ", " }
                   }
                   return forms;
                 }},
                 {"render": function (data, type, row, meta) {
                   let taxs = row[1].split(', ');
                   let forms = "";
                   let i = 1;
                   if ( etype == 'have_produced_2' || etype == 'have_produced_1' || etype == 'has_a_physiological_stage' || etype == 'has_given_birth_since' ) { i = 0; }
                   for ( let i = 1; i < taxs.length ; i++ ) {
                     forms += taxs[i]
                     if ( i != taxs.length - 1 ) { forms += ", " }
                   }
                   return forms;
                 }},
                 {"render": function (data, type, row, meta) {
                    let rtype = '';
                    if ( row[2] == 'Have produced 2' ) { rtype = 'lactationstage'; }
                    else if ( row[2] == 'Have produced 1' ) { rtype = 'sampletype'; }
                    else if ( row[2] == 'Has given birth since' ) { rtype = 'postpartumage'; }
                    else if ( row[2] == 'Has a physiological stage' ) { rtype = 'physiologicalstage'; }
                    else if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
                    else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'use'; }
                    else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
                    var docids = format_docs(row, alvisir, rtype);
                    return docids;
                 }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[8];
                 }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[6];
                 }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[9];
                   }},
                 {"visible": false, "render": function (data, type, row, meta) {
                    return row[7];
                  }}
               ]
              });
        });
}
