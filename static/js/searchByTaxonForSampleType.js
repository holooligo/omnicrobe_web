/**
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
**/

import { format_docs, createTable, createTableWithArg } from './utils.js';

$("#searchByTaxonForSampleType.nav-item").addClass( "active" );

$('#spinner_taxon_sample').show();
$('#spinner_taxon_sample2').show();

var thtable = $('#results_taxon_sample').DataTable();

var $select = $('#search_taxon_sample').selectize({
    valueField: 'path',
    labelField: 'name',
    searchField: 'name',
    sortField: 'name',
    placeholder: 'e.g. Bos taurus',
    openOnFocus: false,
    create: false,
    maxItems: 1,
    onInitialize: function() {
      var that = this;

      // $.getJSON($SCRIPT_ROOT + '/_get_list_term',
      $.getJSON($SCRIPT_ROOT + '/_get_list_obt_class',
        { table: "list_taxon_sample"},
        function(data) {
          data.forEach(function(item) {
            that.addOption(item);
          });
          $('#spinner_taxon_sample').hide();
          $('#spinner_taxon_sample2').hide();
          $('#search_taxon_sample').prop('disabled', false);
        });
    },
    onChange:function(value){
      if (value != "") {

        $('#filter_taxon_sample').removeAttr('disabled');
        $('#spinner_taxon_sample').show();
        $('#spinner_taxon_sample2').show();

        createTable(value, 'have_produced_1', 'results_taxon_sample');

        if ( $('#sample_tu').val() != '' ) { filterColumnsample(3); }
        // if ( $('input[name=qps_tu]').is(':checked') == true ) { filterColumnCheck(4); }

        $('#spinner_taxon_sample').hide();
        $('#spinner_taxon_sample2').hide();

        checkURL();
      }
      else {
        alert("No result for " + taxon);
        // Spinner off
        $('#spinner_taxon_sample').hide();
        $('#spinner_taxon_sample2').hide();
        // Clear oracle
        $("#relationSampleByTaxon").val("");
        $('#filter_taxon_sample').attr('disabled', 'disabled');
        // Change URL
        window.location.replace(window.location.pathname);
      }
		}
  });

var selectize = $select[0].selectize;

// URL: taxon
if ( taxon !== null ) {
  $('#spinner_taxon_sample').show();
  $('#spinner_taxon_sample2').show();
  selectize.setTextboxValue(taxon);
  $("#search_taxon_sample option:selected").text(taxon);
  $("#search_taxon_sample option:selected").val(taxon);
  $('#filter_taxon_sample').removeAttr('disabled');

  $.getJSON($SCRIPT_ROOT + '/_get_path',
    {name: taxon, table: 'list_taxon_sample'},
    function success(path) {
      if ( path != '' ) {
        let l_path = path[0].split('/');
        let taxonid = l_path[l_path.length-1];

        createTableWithArg(taxonid, 'have_produced_1', 'results_taxon_sample');
      }
      else {
        alert("No result for " + taxon);
        // Spinner off
        $('#spinner_taxon_sample').hide();
        $('#spinner_taxon_sample2').hide();
        // Clear oracle
        $("#relationSampleByTaxon").val("");
        $('#filter_taxon_sample').attr('disabled', 'disabled');
        // Change URL
        window.location.replace(window.location.pathname);
      }
  });
}
// URL: qps
// if ( qps !== null ) {
//   $('input:checkbox').prop('checked', true);
// }
// URL: sample
if ( sample !== null ) {
  $('input.column_filter').val(sample);
}

// var thtable = $('#results_taxon_sample').DataTable();

// Data table
// function createTable(path) {
//   if ( path != '' ) {
//         let l_path = path.split('/');
//         let taxonid = l_path[l_path.length-1];
//
//         $('#filter_taxon_sample').removeAttr('disabled');
//         $('#spinner_taxon_sample').show();
//         $('#spinner_taxon_sample2').show();
//
//         $.getJSON($SCRIPT_ROOT + '/_get_list_relations',
//           { taxonid: taxonid,
//             type: 'sample'
//           },
//           function success(relations) {
//             $('#hide').css( 'display', 'block' );
//             $('#results_taxon_sample').DataTable().destroy();
//             thtable = $('#results_taxon_sample').DataTable(
//               {dom: 'lifrtBp', // 'Bfrtip'
//                data: relations,
//                buttons: [
//                           {
//                               extend: 'copyHtml5',
//                               exportOptions: { columns: function ( idx, data, node ) {
//                                                 var table_id = node.getAttribute('aria-controls');
//                                                 if ( idx == 0 ) { return false; } // Never Source Text
//                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
//                                                 return $('#' + table_id).DataTable().column( idx ).visible(); }
//                                              },
//                               title: 'Omnicrobe_V_'+version
//                           },
//                           {
//                               extend: 'csvHtml5',
//                               exportOptions: { columns: function ( idx, data, node ) {
//                                                 var table_id = node.getAttribute('aria-controls');
//                                                 if ( idx == 0 ) { return false; } // Never Source Text
//                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
//                                                 return $('#' + table_id).DataTable().column( idx ).visible(); }
//                                              },
//                               title: 'Omnicrobe_V_'+version
//                           },
//                           {
//                               extend: 'excelHtml5',
//                               exportOptions: { columns: function ( idx, data, node ) {
//                                                  var table_id = node.getAttribute('aria-controls');
//                                                  if ( idx == 0 ) { return false; } // Never Source Text
//                                                  else if ( idx == 6 ) { return true; } // Always Full Source Text
//                                                  else { return $('#' + table_id).DataTable().column( idx ).visible(); } }
//                                              },
//                               title: 'Omnicrobe_V_'+version
//                           },
//                           {
//                               extend: 'pdfHtml5',
//                               exportOptions: { columns: function ( idx, data, node ) {
//                                                  var table_id = node.getAttribute('aria-controls');
//                                                  if ( idx == 0 ) { return false; } // Never Source Text
//                                                  else if ( idx == 6 ) { return true; } // Always Full Source Text
//                                                  return $('#' + table_id).DataTable().column( idx ).visible(); }
//                                              },
//                               title: 'Omnicrobe_V_'+version
//                           },
//                           'colvis'
//                       ],
//                columns: [
//                  {"render": function(data, type, row, meta) {
//                     let rtype = '';
//                     if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
//                     else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'sample'; }
//                     else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
//                     var docids = format_docs(row, alvisir, rtype);
//                     let docs = "";
//                     if ( data.includes(', ') ) { docs = data.split(', '); }
//                     else                       { docs = data.split(','); }
//                     let docs_f = "";
//                     if ( docs.length > 2 ) { // 3
//                       docs_f = docids.split(", ").slice(0,2).join(', ') + ", ..."; // 0,3
//                     }
//                     else {
//                       docs_f = docids;
//                     }
//                     return docs_f;
//                   }},
//                  {"render": function ( data, type, row, meta ) {
//                     let taxa = row[1].split(', ');
//                     let taxon = taxa[0];
//                     if ( row[9].includes("ncbi") ) {
//                       taxon = "<a target='_blank' class='doc' href='https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id="+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
//                     }
//                     else if ( row[9].includes("bd") ) {
//                       taxon = "<a target='_blank' class='doc' href='https://bacdive.dsmz.de/strain/"+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
//                     }
//                     return taxon;
//                   }},
//                  {"render": function (data, type, row, meta) {
//                      return row[2];
//                  }},
//                  {"render": function (data, type, row, meta) {
//                      return row[3].split(',')[0];
//                  }},
//                  {"orderable": false, "render": function (data, type, row, meta) {
//                      return row[4];
//                  }},
//                  {"render": function (data, type, row, meta) {
//                      return row[5];
//                  }},
//                  {"render": function (data, type, row, meta) {
//                     let taxs = row[1].split(', ');
//                     let forms = "";
//                     for ( let i = 1; i < taxs.length ; i++ ) {
//                       forms += taxs[i]
//                       if ( i != taxs.length - 1 ) { forms += ", " }
//                     }
//                     return forms;
//                  }},
//                  {"render": function (data, type, row, meta) {
//                      let elts = row[3].split(', ');
//                      let forms = "";
//                      for ( let i = 1; i < elts.length ; i++ ) {
//                        forms += elts[i]
//                        if ( i != elts.length - 1 ) { forms += ", " }
//                      }
//                      return forms;
//                  }},
//                  {"render": function (data, type, row, meta) {
//                     let rtype = '';
//                     if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
//                     else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'sample'; }
//                     else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
//                     var docids = format_docs(row, alvisir, rtype);
//                     return docids;
//                  }},
//                  {"visible": false, "render": function (data, type, row, meta) {
//                      return row[9];
//                    }},
//                  {"visible": false, "render": function (data, type, row, meta) {
//                     return row[7];
//                   }},
//                  {"visible": false, "render": function (data, type, row, meta) {
//                      return row[8];
//                  }},
//                  {"visible": false, "render": function (data, type, row, meta) {
//                      return row[6];
//                  }}
//                ]
//               });
//
//               if ( $('#sample_tu').val() != '' ) { filterColumnsample(3); }
//               if ( $('input[name=qps_tu]').is(':checked') == true ) { filterColumnCheck(4); }
//
//               $('#spinner_taxon_sample').hide();
//               $('#spinner_taxon_sample2').hide();
//
//               checkURL();
//         });
//       }
//   else {
//         alert("No result for " + taxon);
//         // Spinner off
//         $('#spinner_taxon_sample').hide();
//         $('#spinner_taxon_sample2').hide();
//         // Clear oracle
//         $("#relationsampleByTaxon").val("");
//         $('#filter_taxon_sample').attr('disabled', 'disabled');
//         // Change URL
//         window.location.replace(window.location.pathname);
//       }
// }
// function createTable_old(taxon) {
//   $.getJSON($SCRIPT_ROOT + '/_get_path',
//     {name: taxon, table: 'list_taxon_sample'},
//     function success(path) {
//       if ( path != '' ) {
//         let l_path = path[0].split('/');
//         let taxonid = l_path[l_path.length-1];
//
//         $.getJSON($SCRIPT_ROOT + '/_get_list_relations',
//           { taxonid: taxonid,
//             type: 'sample'
//           },
//           function success(relations) {
//             $('#hide').css( 'display', 'block' );
//             $('#results_taxon_sample').DataTable().destroy();
//             thtable = $('#results_taxon_sample').DataTable(
//               {dom: 'lifrtBp', // 'Bfrtip'
//                data: relations,
//                buttons: [
//                           {
//                               extend: 'copyHtml5',
//                               exportOptions: { columns: function ( idx, data, node ) {
//                                                 var table_id = node.getAttribute('aria-controls');
//                                                 if ( idx == 0 ) { return false; } // Never Source Text
//                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
//                                                 return $('#' + table_id).DataTable().column( idx ).visible(); }
//                                              },
//                               title: 'Omnicrobe_V_'+version
//                           },
//                           {
//                               extend: 'csvHtml5',
//                               exportOptions: { columns: function ( idx, data, node ) {
//                                                 var table_id = node.getAttribute('aria-controls');
//                                                 if ( idx == 0 ) { return false; } // Never Source Text
//                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
//                                                 return $('#' + table_id).DataTable().column( idx ).visible(); }
//                                              },
//                               title: 'Omnicrobe_V_'+version
//                           },
//                           {
//                               extend: 'excelHtml5',
//                               exportOptions: { columns: function ( idx, data, node ) {
//                                                  var table_id = node.getAttribute('aria-controls');
//                                                  if ( idx == 0 ) { return false; } // Never Source Text
//                                                  else if ( idx == 6 ) { return true; } // Always Full Source Text
//                                                  else { return $('#' + table_id).DataTable().column( idx ).visible(); } }
//                                              },
//                               title: 'Omnicrobe_V_'+version
//                           },
//                           {
//                               extend: 'pdfHtml5',
//                               exportOptions: { columns: function ( idx, data, node ) {
//                                                  var table_id = node.getAttribute('aria-controls');
//                                                  if ( idx == 0 ) { return false; } // Never Source Text
//                                                  else if ( idx == 6 ) { return true; } // Always Full Source Text
//                                                  return $('#' + table_id).DataTable().column( idx ).visible(); }
//                                              },
//                               title: 'Omnicrobe_V_'+version
//                           },
//                           'colvis'
//                       ],
//                columns: [
//                  {"render": function(data, type, row, meta) {
//                     let rtype = '';
//                     if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
//                     else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'sample'; }
//                     else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
//                     var docids = format_docs(row, alvisir, rtype);
//                     let docs = "";
//                     if ( data.includes(', ') ) { docs = data.split(', '); }
//                     else                       { docs = data.split(','); }
//                     let docs_f = "";
//                     if ( docs.length > 2 ) { // 3
//                       docs_f = docids.split(", ").slice(0,2).join(', ') + ", ..."; // 0,3
//                     }
//                     else {
//                       docs_f = docids;
//                     }
//                     return docs_f;
//                   }},
//                  {"render": function ( data, type, row, meta ) {
//                     let taxa = row[1].split(', ');
//                     let taxon = taxa[0];
//                     if ( row[9].includes("ncbi") ) {
//                       taxon = "<a target='_blank' class='doc' href='https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id="+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
//                     }
//                     else if ( row[9].includes("bd") ) {
//                       taxon = "<a target='_blank' class='doc' href='https://bacdive.dsmz.de/strain/"+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
//                     }
//                     return taxon;
//                   }},
//                  {"render": function (data, type, row, meta) {
//                      return row[2];
//                  }},
//                  {"render": function (data, type, row, meta) {
//                      return row[3].split(',')[0];
//                  }},
//                  {"orderable": false, "render": function (data, type, row, meta) {
//                      return row[4];
//                  }},
//                  {"render": function (data, type, row, meta) {
//                      return row[5];
//                  }},
//                  {"render": function (data, type, row, meta) {
//                     let taxs = row[1].split(', ');
//                     let forms = "";
//                     for ( let i = 1; i < taxs.length ; i++ ) {
//                       forms += taxs[i]
//                       if ( i != taxs.length - 1 ) { forms += ", " }
//                     }
//                     return forms;
//                  }},
//                  {"render": function (data, type, row, meta) {
//                      let elts = row[3].split(', ');
//                      let forms = "";
//                      for ( let i = 1; i < elts.length ; i++ ) {
//                        forms += elts[i]
//                        if ( i != elts.length - 1 ) { forms += ", " }
//                      }
//                      return forms;
//                  }},
//                  {"render": function (data, type, row, meta) {
//                     let rtype = '';
//                     if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
//                     else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'sample'; }
//                     else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
//                     var docids = format_docs(row, alvisir, rtype);
//                     return docids;
//                  }},
//                  {"visible": false, "render": function (data, type, row, meta) {
//                      return row[9];
//                    }},
//                  {"visible": false, "render": function (data, type, row, meta) {
//                     return row[7];
//                   }},
//                  {"visible": false, "render": function (data, type, row, meta) {
//                      return row[8];
//                  }},
//                  {"visible": false, "render": function (data, type, row, meta) {
//                      return row[6];
//                  }}
//                ]
//               });
//
//               if ( $('#sample_tu').val() != '' ) { filterColumnsample(3); }
//               if ( $('input[name=qps_tu]').is(':checked') == true ) { filterColumnCheck(4); }
//
//               $('#spinner_taxon_sample').hide();
//               $('#spinner_taxon_sample2').hide();
//
//               checkURL();
//         });
//       }
//       else {
//         alert("No result for " + taxon);
//         // Spinner off
//         $('#spinner_taxon_sample').hide();
//         $('#spinner_taxon_sample2').hide();
//         // Clear oracle
//         $("#relationsampleByTaxon").val("");
//         $('#filter_taxon_sample').attr('disabled', 'disabled');
//         // Change URL
//         window.location.replace(window.location.pathname);
//       }
//   });
// }

// Filter - sample
function filterColumnsample(i) {
  $('#results_taxon_sample').DataTable().column(i).search(
    $('#sample_tu').val().replace(/;/g, "|"), true, false
  ).draw();
  checkURL();
}
$('input.column_filter').on( 'keyup click', function () {
  filterColumnsample($(this).parents('tr').attr('data-column'));
} );

// Filter - QPS
// function filterColumnCheck(i) {
//   let state = $('input[name=qps_tu]').is(':checked');
//   let qps = "";
//   if ( state == true )  { qps = "yes"; }
//   $('#results_taxon_sample').DataTable().column(i).search(
//     qps, true, false
//   ).draw();
//   checkURL();
// }
// $('input:checkbox').on('change', function () {
//     filterColumnCheck(4);
//  });

// Check url
function checkURL() {
  var url = window.location.pathname;
  if ( $("#search_taxon_sample option:selected").text() !== '' ) {
    url += "?taxon=" + $("#search_taxon_sample option:selected").text();
  }
  if ( $("#sample_tu").val() !== '' ) {
    url += "&sample=" + $("#sample_tu").val();
  }
  // if ( $('#qps_tu').is(":checked") ) {
  //   url += "&qps=yes";
  // }
  history.pushState({}, null, url);
}
