taxon_pt/**
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
**/

import { format_docs, createTableRev, createTableWithArgRev } from './utils.js';

$("#searchByPostpartum.nav-item").addClass( "active" );

// Spinner
$('#spinner_postpartum_taxon').show();
$('#spinner_postpartum_taxon2').show();

var thtable = $('#results_postpartum_taxon').DataTable();

var $select = $('#search_postpartum').selectize({
    valueField: 'path',
    labelField: 'name',
    searchField: 'name',
    sortField: 'name',
    placeholder: 'e.g. first week postpartum',
    openOnFocus: false,
    create: false,
    maxItems: 1,
    onInitialize: function() {
      var that = this;

      $.getJSON($SCRIPT_ROOT + '/_get_list_obt_class',
        { table: "list_postpartum_taxon"},
        function(data) {
          data.forEach(function(item) {
            that.addOption(item);
          });
          $('#spinner_postpartum_taxon').hide();
          $('#spinner_postpartum_taxon2').hide();
          $('#search_postpartum option:selected').prop('disabled', false);
        });
    },
    onChange:function(value){
      if (value != '') {

        $('#filter_postpartum_taxon').removeAttr('disabled');
        $('#spinner_postpartum_taxon').show();
        $('#spinner_postpartum_taxon2').show();

        createTableRev(value, 'has_given_birth_since', 'results_postpartum_taxon');

        if ( $('#taxon_pt').val() != '' ) { filterColumntaxon(3); }
        // if ( $('#sources_th').val() != '' ) { filterSourceColumn(5); }
        // if ( $('input[name=qps_th]').is(':checked') == true ) { filterColumnCheck(4); }

        $('#spinner_postpartum_taxon').hide();
        $('#spinner_postpartum_taxon2').hide();

        checkURL();
      }
      else {
            alert("No result for " + taxon);
            // Spinner off
            $('#spinner_postpartum_taxon').hide();
            $('#spinner_postpartum_taxon2').hide();
            // Clear oracle
            $("#relationTaxonByPostpartum").val("");
            $('#filter_postpartum_taxon').attr('disabled', 'disabled');
            // Change URL
            window.location.replace(window.location.pathname);
          }
		}
  });

var selectize = $select[0].selectize;

// URL: taxon
if ( postpartum !== null ) {
  $('#spinner_postpartum_taxon').show();
  $('#spinner_postpartum_taxon2').show();
  selectize.setTextboxValue(postpartum);
  $("#search_postpartum option:selected").text(postpartum);
  $("#search_postpartum option:selected").val(postpartum);
  $('#filter_postpartum_taxon').removeAttr('disabled');

  $.getJSON($SCRIPT_ROOT + '/_get_path',
    {name: taxon, table: 'list_postpartum_taxon'},
    function success(path) {
      if ( path != '' ) {
        let l_path = path[0].split('/');
        let postpartumid = l_path[l_path.length-1];

        createTableWithArgRev(postpartumid, 'has_given_birth_since', 'results_postpartum_taxon');

        if ( $('#taxon_pt').val() != '' ) { filterColumntaxon(3); }
        // if ( $('#sources_th').val() != '' ) { filterSourceColumn(5); }
        // if ( $('input[name=qps_th]').is(':checked') == true ) { filterColumnCheck(4); }

        $('#spinner_postpartum_taxon').hide();
        $('#spinner_postpartum_taxon2').hide();

        checkURL();
      }
      else {
        alert("No result for " + taxon);
        // Spinner off
        $('#spinner_postpartum_taxon').hide();
        $('#spinner_postpartum_taxon2').hide();
        // Clear oracle
        $("#relationTaxonByPostpartum").val("");
        $('#filter_postpartum_taxon').attr('disabled', 'disabled');
        // Change URL
        window.location.replace(window.location.pathname);
      }
  });
}
// URL: qps
// if ( qps !== null ) {
//   $('input:checkbox').prop('checked', true);
// }
// URL: sources
// if ( sources !== null ) {
//   $('input.column_source').val(sources.join(";"));
// }
// URL: postpartum
if ( taxon !== null ) {
  $('input.column_filter').val(taxon);
}

// Filter - taxon
function filterColumntaxon(i) {
  $('#results_postpartum_taxon').DataTable().column(i).search(
    $('#taxon_pt').val().replace(/;/g, "|"), true, false
  ).draw();
  checkURL();
}
$('input.column_filter').on( 'keyup click', function () {
  filterColumntaxon($(this).parents('tr').attr('data-column'));
} );

// Filter - QPS
// function filterColumnCheck(i) {
//   let state = $('input[name=qps_th]').is(':checked');
//   let qps = "";
//   if ( state == true )  { qps = "yes"; }
//   $('#results_taxon_stage').DataTable().column(i).search(
//     qps, true, false
//   ).draw();
//   checkURL();
// }
// $('input:checkbox').on('change', function () {
//     filterColumnCheck(4);
//  });

 // Filter - Sources
// function filterSourceColumn(i) {
//    $('#results_taxon_stage').DataTable().column(i).search(
//      $('#sources_th').val().replace(/;/g, "|"), true, false
//    ).draw();
//    checkURL();
// }
// $('input.column_source').on( 'keyup click', function () {
//    filterSourceColumn(5);
// });

// Check url
function checkURL() {
  var url = window.location.pathname;
  if ( $("#search_postpartum option:selected").text() !== '' ) {
    url += "?postpartum=" + $("#search_postpartum option:selected").text();
  }
  if ( $("#taxon_pt").val() !== '' ) {
    url += "&taxon=" + $("#taxon_pt").val();
  }
  // if ( $('#qps_th').is(":checked") ) {
  //   url += "&qps=yes";
  // }
  // if ( $("#sources_th").val() !== '' ) {
  //   if ( $("#sources_th").val().includes(";") ) {
  //     let list = $("#sources_th").val().split(";");
  //     for (let s = 0 ; s < list.length ; s++ ) {
  //       if ( list[s] != '' ) {
  //         url += "&source=" + list[s];
  //       }
  //     }
  //   }
  //   else {
  //     url += "&source=" + $("#sources_th").val();
  //   }
  // }
  history.pushState({}, null, url);
}
