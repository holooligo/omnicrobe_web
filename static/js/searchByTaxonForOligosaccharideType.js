/**
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
**/

import { format_docs, createTable, createTableWithArg } from './utils.js';

$("#searchByTaxonForOligosaccharideType.nav-item").addClass( "active" );

// Spinner
$('#spinner_taxon_oligotype').show();
$('#spinner_taxon_oligotype2').show();

var thtable = $('#results_taxon_oligotype').DataTable();

var $select = $('#search').selectize({
    valueField: 'path',
    labelField: 'name',
    searchField: 'name',
    sortField: 'name',
    placeholder: 'e.g. Bos taurus',
    openOnFocus: false,
    create: false,
    maxItems: 1,
    onInitialize: function() {
      var that = this;

      $.getJSON($SCRIPT_ROOT + '/_get_list_obt_class',
        { table: "list_taxon_oligo_type"},
        function(data) {
          data.forEach(function(item) {
            that.addOption(item);
          });
          $('#spinner_taxon_oligotype').hide();
          $('#spinner_taxon_oligotype2').hide();
          $('#search option:selected').prop('disabled', false);
        });
    },
    onChange:function(value){
      if (value != '') {

        $('#filter_taxon_oligotype').removeAttr('disabled');
        $('#spinner_taxon_oligotype').show();
        $('#spinner_taxon_oligotype2').show();

        createTable(value, 'have_produced_tmp_2', 'results_taxon_oligotype');

        if ( $('#oligotype_th').val() != '' ) { filterColumnoligotype(3); }

        $('#spinner_taxon_oligotype').hide();
        $('#spinner_taxon_oligotype2').hide();

        checkURL();
      }
      else {
            alert("No result for " + taxon);
            // Spinner off
            $('#spinner_taxon_oligotype').hide();
            $('#spinner_taxon_oligotype2').hide();
            // Clear oracle
            $("#relationoligotypeByTaxon").val("");
            $('#filter_taxon_oligotype').attr('disabled', 'disabled');
            // Change URL
            window.location.replace(window.location.pathname);
          }
		}
  });

var selectize = $select[0].selectize;

// URL: taxon
if ( taxon !== null ) {
  $('#spinner_taxon_oligotype').show();
  $('#spinner_taxon_oligotype2').show();
  selectize.setTextboxValue(taxon);
  $("#search option:selected").text(taxon);
  $("#search option:selected").val(taxon);
  $('#filter_taxon_oligotype').removeAttr('disabled');

  $.getJSON($SCRIPT_ROOT + '/_get_path',
    {name: taxon, table: 'list_taxon_oligo_type'},
    function success(path) {
      if ( path != '' ) {
        let l_path = path[0].split('/');
        let taxonid = l_path[l_path.length-1];

        createTableWithArg(taxonid, 'have_produced_tmp_2', 'results_taxon_oligotype');

        if ( $('#oligotype_th').val() != '' ) { filterColumnoligotype(3); }

        $('#spinner_taxon_oligotype').hide();
        $('#spinner_taxon_oligotype2').hide();

        checkURL();
      }
      else {
        alert("No result for " + taxon);
        // Spinner off
        $('#spinner_taxon_oligotype').hide();
        $('#spinner_taxon_oligotype2').hide();
        // Clear oracle
        $("#relationvByTaxon").val("");
        $('#filter_taxon_oligotype').attr('disabled', 'disabled');
        // Change URL
        window.location.replace(window.location.pathname);
      }
  });
}

// URL: oligotype
if ( oligotype !== null ) {
  $('input.column_filter').val(oligotype);
}

// Filter - oligotype
function filterColumnoligotype(i) {
  $('#results_taxon_oligotype').DataTable().column(i).search(
    $('#oligotype_th').val().replace(/;/g, "|"), true, false
  ).draw();
  checkURL();
}
$('input.column_filter').on( 'keyup click', function () {
  filterColumnoligotype($(this).parents('tr').attr('data-column'));
} );

// Check url
function checkURL() {
  var url = window.location.pathname;
  if ( $("#search option:selected").text() !== '' ) {
    url += "?taxon=" + $("#search option:selected").text();
  }
  if ( $("#oligotype_th").val() !== '' ) {
    url += "&oligotype=" + $("#oligotype_th").val();
  }
  history.pushState({}, null, url);
}
