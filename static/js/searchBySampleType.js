/**
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
**/

import { format_docs, createTableRev, createTableWithArgRev } from './utils.js';

$("#searchBySampleType.nav-item").addClass( "active" );

// Spinner
$('#spinner_sample_taxon').show();
$('#spinner_sample_taxon2').show();

var thtable = $('#results_sample_taxon').DataTable();

var $select = $('#search_sample').selectize({
    valueField: 'path',
    labelField: 'name',
    searchField: 'name',
    sortField: 'name',
    placeholder: 'e.g. Pooled milk',
    openOnFocus: false,
    create: false,
    maxItems: 1,
    onInitialize: function() {
      var that = this;

      $.getJSON($SCRIPT_ROOT + '/_get_list_obt_class',
        { table: "list_sample_taxon"},
        function(data) {
          data.forEach(function(item) {
            that.addOption(item);
          });
          $('#spinner_sample_taxon').hide();
          $('#spinner_sample_taxon2').hide();
          $('#search_sample option:selected').prop('disabled', false);
        });
    },
    onChange:function(value){
      if (value != '') {

        $('#filter_sample_taxon').removeAttr('disabled');
        $('#spinner_sample_taxon').show();
        $('#spinner_sample_taxon2').show();

        createTableRev(value, 'have_produced_1', 'results_sample_taxon');

        if ( $('#taxon_ut').val() != '' ) { filterColumntaxon(3); }
        // if ( $('#sources_th').val() != '' ) { filterSourceColumn(5); }
        // if ( $('input[name=qps_th]').is(':checked') == true ) { filterColumnCheck(4); }

        $('#spinner_sample_taxon').hide();
        $('#spinner_sample_taxon2').hide();

        checkURL();
      }
      else {
            alert("No result for " + taxon);
            // Spinner off
            $('#spinner_sample_taxon').hide();
            $('#spinner_sample_taxon2').hide();
            // Clear oracle
            $("#relationTaxonBySample").val("");
            $('#filter_sample_taxon').attr('disabled', 'disabled');
            // Change URL
            window.location.replace(window.location.pathname);
          }
		}
  });

var selectize = $select[0].selectize;

// URL: taxon
if ( sample !== null ) {
  $('#spinner_sample_taxon').show();
  $('#spinner_sample_taxon2').show();
  selectize.setTextboxValue(sample);
  $("#search_sample option:selected").text(sample);
  $("#search_sample option:selected").val(sample);
  $('#filter_sample_taxon').removeAttr('disabled');

  $.getJSON($SCRIPT_ROOT + '/_get_path',
    {name: taxon, table: 'list_sample_taxon'},
    function success(path) {
      if ( path != '' ) {
        let l_path = path[0].split('/');
        let sampleid = l_path[l_path.length-1];

        createTableWithArgRev(sampleid, 'have_produced_1', 'results_sample_taxon');

        if ( $('#taxon_ut').val() != '' ) { filterColumntaxon(3); }
        // if ( $('#sources_th').val() != '' ) { filterSourceColumn(5); }
        // if ( $('input[name=qps_th]').is(':checked') == true ) { filterColumnCheck(4); }

        $('#spinner_sample_taxon').hide();
        $('#spinner_sample_taxon2').hide();

        checkURL();
      }
      else {
        alert("No result for " + taxon);
        // Spinner off
        $('#spinner_sample_taxon').hide();
        $('#spinner_sample_taxon2').hide();
        // Clear oracle
        $("#relationTaxonBySample").val("");
        $('#filter_sample_taxon').attr('disabled', 'disabled');
        // Change URL
        window.location.replace(window.location.pathname);
      }
  });
}
// URL: qps
// if ( qps !== null ) {
//   $('input:checkbox').prop('checked', true);
// }
// URL: sources
// if ( sources !== null ) {
//   $('input.column_source').val(sources.join(";"));
// }
// URL: sample
if ( taxon !== null ) {
  $('input.column_filter').val(taxon);
}

// Filter - taxon
function filterColumntaxon(i) {
  $('#results_sample_taxon').DataTable().column(i).search(
    $('#taxon_ut').val().replace(/;/g, "|"), true, false
  ).draw();
  checkURL();
}
$('input.column_filter').on( 'keyup click', function () {
  filterColumntaxon($(this).parents('tr').attr('data-column'));
} );

// Filter - QPS
// function filterColumnCheck(i) {
//   let state = $('input[name=qps_th]').is(':checked');
//   let qps = "";
//   if ( state == true )  { qps = "yes"; }
//   $('#results_taxon_stage').DataTable().column(i).search(
//     qps, true, false
//   ).draw();
//   checkURL();
// }
// $('input:checkbox').on('change', function () {
//     filterColumnCheck(4);
//  });

 // Filter - Sources
// function filterSourceColumn(i) {
//    $('#results_taxon_stage').DataTable().column(i).search(
//      $('#sources_th').val().replace(/;/g, "|"), true, false
//    ).draw();
//    checkURL();
// }
// $('input.column_source').on( 'keyup click', function () {
//    filterSourceColumn(5);
// });

// Check url
function checkURL() {
  var url = window.location.pathname;
  if ( $("#search_sample option:selected").text() !== '' ) {
    url += "?sample=" + $("#search_sample option:selected").text();
  }
  if ( $("#taxon_ut").val() !== '' ) {
    url += "&taxon=" + $("#taxon_ut").val();
  }
  // if ( $('#qps_th').is(":checked") ) {
  //   url += "&qps=yes";
  // }
  // if ( $("#sources_th").val() !== '' ) {
  //   if ( $("#sources_th").val().includes(";") ) {
  //     let list = $("#sources_th").val().split(";");
  //     for (let s = 0 ; s < list.length ; s++ ) {
  //       if ( list[s] != '' ) {
  //         url += "&source=" + list[s];
  //       }
  //     }
  //   }
  //   else {
  //     url += "&source=" + $("#sources_th").val();
  //   }
  // }
  history.pushState({}, null, url);
}
