/**
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
**/

import { format_docs } from './utils.js';

$("#searchByUse.nav-item").addClass( "active" );

$('#spinner_use_taxon').show();
$('#spinner_use_taxon2').show();

// Use selection
var $select = $('#search_use').selectize({
    valueField: 'path',
    labelField: 'name',
    searchField: 'name',
    sortField: 'name',
    placeholder: 'e.g. acidification',
    openOnFocus: false,
    create: false,
    maxItems: 1,
    onInitialize: function() {
      var that = this;

      // $.getJSON($SCRIPT_ROOT + '/_get_list_term',
      $.getJSON($SCRIPT_ROOT + '/_get_list_obt_class',
        { table: "list_use_taxon"},
        function(data) {
          data.forEach(function(item) {
            that.addOption(item);
          });
          $('#spinner_use_taxon').hide();
          $('#spinner_use_taxon2').hide();
          $('#search_use option:selected').prop('disabled', false);
        });
    },
    onChange:function(ontobiotopeid){
      if (ontobiotopeid != "") {

        // Tree event
        $('#usetree').data('simpleTree').clearSelection();
        $('#usetree').data('simpleTree').collapseAll();
        var node = $('#usetree').data('simpleTree').getNodeFromValue(ontobiotopeid);
        $('#usetree').data('simpleTree').setSelectedNode(node);
      }
		}
  });
var selectize = $select[0].selectize;

// Tree
var options = {
    searchBox: $('#searchtreeuse'),
    searchMinInputLength: 4
};
$.getJSON($SCRIPT_ROOT + '/_get_ontobiotope_use', function(data) {
  $('#usetree').simpleTree(options, data);

  // URL: use
  if ( use !== null ) {
    // $('#spinner_use_taxon').show();
    // $('#spinner_use_taxon2').show();
    //
    // $("#relationTaxonByUse").val(use);
    // $('#filter_use_taxon').removeAttr('disabled');

    $.getJSON($SCRIPT_ROOT + '/_get_path',
      {name: use, table: 'list_use_taxon'},
      function success(path) {
        if ( path != '' ) {
          let l_path = path[0].split('/');
          let ontobiotopeid = l_path[l_path.length-1];

          // Tree event
          var node = $('#usetree').data('simpleTree').getNodeFromValue(ontobiotopeid);
          $('#usetree').data('simpleTree').setSelectedNode(node);
          // $('#searchtreeuse').val(use);
        }
      });

    // createTable(use);
  }

  // URL: qps
  if ( qps !== null ) {
    $('input:checkbox').prop('checked', true);
  }

  // URL: taxon
  if ( taxon !== null ) {
    $('input.column_filter').val(taxon);
  }
});
$('#usetree').on('simpleTree:change', function(event){

  if ( $('#usetree').data('simpleTree').getSelectedNode() != undefined ) {

    selectize.clear(false);
    selectize.setValue($('#usetree').data('simpleTree').getSelectedNode().value, true);

    $('#spinner_use_taxon').show();
    $('#spinner_use_taxon2').show();

    $('#searchtreeuse').val($('#usetree').data('simpleTree').getSelectedNode().label);
    createTable($('#usetree').data('simpleTree').getSelectedNode().value);
    $("#search_use option:selected").text();
    $("#search_use option:selected").text($('#usetree').data('simpleTree').getSelectedNode().label);
    $("#search_use option:selected").val($('#usetree').data('simpleTree').getSelectedNode().value);
    //selectize.setTextboxValue("");
    //selectize.setTextboxValue($('#usetree').data('simpleTree').getSelectedNode().label);
    $('#filter_use_taxon').removeAttr('disabled');
    $('#treeModalUse').modal('hide');
  }
})

// Datatable
var thtable = $('#results_use_taxon').DataTable();
function createTable(ontobiotopeid) {

        $.getJSON($SCRIPT_ROOT + '/_get_list_relations',
          {
            ontobiotopeid: ontobiotopeid,
            type: 'use'
          },
          function success(relations) {
            $('#hide').css( 'display', 'block' );
            $('#results_use_taxon').DataTable().destroy();
            thtable = $('#results_use_taxon').DataTable(
              {dom: 'lifrtBp', // 'Bfrtip'
               data: relations,
               buttons: [
                          {
                              extend: 'copyHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                var table_id = node.getAttribute('aria-controls');
                                                if ( idx == 0 ) { return false; } // Never Source Text
                                                else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'csvHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                var table_id = node.getAttribute('aria-controls');
                                                if ( idx == 0 ) { return false; } // Never Source Text
                                                else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'excelHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                 var table_id = node.getAttribute('aria-controls');
                                                 if ( idx == 0 ) { return false; } // Never Source Text
                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                 else { return $('#' + table_id).DataTable().column( idx ).visible(); } }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          {
                              extend: 'pdfHtml5',
                              exportOptions: { columns: function ( idx, data, node ) {
                                                 var table_id = node.getAttribute('aria-controls');
                                                 if ( idx == 0 ) { return false; } // Never Source Text
                                                 else if ( idx == 6 ) { return true; } // Always Full Source Text
                                                 return $('#' + table_id).DataTable().column( idx ).visible(); }
                                             },
                              title: 'Omnicrobe_V_'+version
                          },
                          'colvis'
                      ],
               columns: [
                 {"render": function(data, type, row, meta) {
                    let rtype = '';
                    if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
                    else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'use'; }
                    else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
                    var docids = format_docs(row, alvisir, rtype);
                    let docs = "";
                    if ( data.includes(', ') ) { docs = data.split(', '); }
                    else                       { docs = data.split(','); }
                    let docs_f = "";
                    if ( docs.length > 2 ) { // 3
                      docs_f = docids.split(", ").slice(0,2).join(', ') + ", ..."; // 0,3
                    }
                    else {
                      docs_f = docids;
                    }
                    return docs_f;
                  }},
                 {"render": function (data, type, row, meta) {
                     return row[3].split(',')[0];
                 }},
                 {"render": function (data, type, row, meta) {
                     return row[2];
                 }},
                 {"render": function ( data, type, row, meta ) {
                   let taxa = row[1].split(', ');
                   let taxon = taxa[0];
                   if ( row[9].includes("ncbi") ) {
                     taxon = "<a target='_blank' class='doc' href='https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id="+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
                   }
                   else if ( row[9].includes("bd") ) {
                     taxon = "<a target='_blank' class='doc' href='https://bacdive.dsmz.de/strain/"+row[9].replace(/.+:/ig, '')+"'>"+taxa[0]+"</a>";
                   }
                   return taxon;
                  }},
                 {"orderable": false, "render": function (data, type, row, meta) {
                     return row[4];
                 }},
                 {"render": function (data, type, row, meta) {
                     return row[5];
                 }},
                 {"render": function (data, type, row, meta) {
                     let elts = row[3].split(', ');
                     let forms = "";
                     for ( let i = 1; i < elts.length ; i++ ) {
                       forms += elts[i]
                       if ( i != elts.length - 1 ) { forms += ", " }
                     }
                     return forms;
                 }},
                 {"render": function (data, type, row, meta) {
                    let taxs = row[1].split(', ');
                    let forms = "";
                    for ( let i = 1; i < taxs.length ; i++ ) {
                      forms += taxs[i]
                      if ( i != taxs.length - 1 ) { forms += ", " }
                    }
                    return forms;
                 }},
                 {"render": function (data, type, row, meta) {
                    let rtype = '';
                    if ( row[2] == 'Lives in' || row[2] == 'Contains' ) { rtype = 'habitat'; }
                    else if ( row[2] == 'Studied for' || row[2] == 'Involves' ) { rtype = 'use'; }
                    else if ( row[2] == 'Exhibits' || row[2] == 'Is exhibited by' ) { rtype = 'phenotype'; }
                    var docids = format_docs(row, alvisir, rtype);
                    return docids;
                 }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[8];
                 }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[6];
                 }},
                 {"visible": false, "render": function (data, type, row, meta) {
                     return row[9];
                   }},
                 {"visible": false, "render": function (data, type, row, meta) {
                    return row[7];
                  }}
               ]
              });

            if ( $('#taxon_ut').val() != '' ) { filterColumnTaxon(3); }
            if ( $('input[name=qps_ut]').is(':checked') == true ) { filterColumnCheck(4); }

            $('#spinner_use_taxon').hide();
            $('#spinner_use_taxon2').hide();

            checkURL();
        });
}

// Filter - Taxon
function filterColumnTaxon(i) {
  $('#results_use_taxon').DataTable().column(i).search(
    $('#taxon_ut').val().replace(/;/g, "|"), true, false
  ).draw();
  checkURL();
}
$('input.column_filter').on( 'keyup click', function () {
  filterColumnTaxon($(this).parents('tr').attr('data-column'));
} );

// Filter - QPS
function filterColumnCheck(i) {
  let state = $('input[name=qps_ut]').is(':checked');
  let qps = "";
  if ( state == true )  { qps = "yes"; }
  $('#results_use_taxon').DataTable().column(i).search(
    qps, true, false
  ).draw();
  checkURL();
}
$('input:checkbox').on('change', function () {
    filterColumnCheck(4);
 });

 // Check url
 function checkURL() {
   var url = window.location.pathname;
   if ( $("#search_use option:selected").text() !== '' ) {
     url += "?use=" + $("#search_use option:selected").text();
   }
   if ( $("#taxon_ut").val() !== '' ) {
     url += "&taxon=" + $("#taxon_ut").val();
   }
   if ( $('#qps_ut').is(":checked") ) {
     url += "&qps=yes";
   }
   history.pushState({}, null, url);
 }
