taxon_lt/**
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
**/

import { format_docs, createTableRev, createTableWithArgRev } from './utils.js';

$("#searchByOligosaccharideType.nav-item").addClass( "active" );

// Spinner
$('#spinner_oligotype_taxon').show();
$('#spinner_oligotype_taxon2').show();

var thtable = $('#results_oligotype_taxon').DataTable();

var $select = $('#search_oligotype').selectize({
    valueField: 'path',
    labelField: 'name',
    searchField: 'name',
    sortField: 'name',
    placeholder: 'e.g. Fucosylated',
    openOnFocus: false,
    create: false,
    maxItems: 1,
    onInitialize: function() {
      var that = this;

      $.getJSON($SCRIPT_ROOT + '/_get_list_obt_class',
        { table: "list_oligo_type_taxon"},
        function(data) {
          data.forEach(function(item) {
            that.addOption(item);
          });
          $('#spinner_oligotype_taxon').hide();
          $('#spinner_oligotype_taxon2').hide();
          $('#search_oligotype option:selected').prop('disabled', false);
        });
    },
    onChange:function(value){
      if (value != '') {

        $('#filter_oligotype_taxon').removeAttr('disabled');
        $('#spinner_oligotype_taxon').show();
        $('#spinner_oligotypee_taxon2').show();

        createTableRev(value, 'have_produced_tmp_2', 'results_oligotype_taxon');

        if ( $('#taxon_lt').val() != '' ) { filterColumntaxon(3); }

        $('#spinner_oligotype_taxon').hide();
        $('#spinner_oligotype_taxon2').hide();

        checkURL();
      }
      else {
            alert("No result for " + taxon);
            // Spinner off
            $('#spinner_oligotype_taxon').hide();
            $('#spinner_oligotype_taxon2').hide();
            // Clear oracle
            $("#relationTaxonByOligosaccharideType").val("");
            $('#filter_oligotype_taxon').attr('disabled', 'disabled');
            // Change URL
            window.location.replace(window.location.pathname);
          }
		}
  });

var selectize = $select[0].selectize;

// URL: taxon
if ( oligotype !== null ) {
  $('#spinner_oligotype_taxon').show();
  $('#spinner_oligotype_taxon2').show();
  selectize.setTextboxValue(oligotype);
  $("#search_oligotype option:selected").text(oligotype);
  $("#search_oligotype option:selected").val(oligotype);
  $('#filter_oligotype_taxon').removeAttr('disabled');

  $.getJSON($SCRIPT_ROOT + '/_get_path',
    {name: taxon, table: 'list_oligo_type_taxon'},
    function success(path) {
      if ( path != '' ) {
        let l_path = path[0].split('/');
        let oligotypeid = l_path[l_path.length-1];

        createTableWithArgRev(oligotypeid, 'have_produced_2', 'results_oligotype_taxon');

        if ( $('#taxon_lt').val() != '' ) { filterColumntaxon(3); }

        $('#spinner_oligotype_taxon').hide();
        $('#spinner_oligotype_taxon2').hide();

        checkURL();
      }
      else {
        alert("No result for " + taxon);
        // Spinner off
        $('#spinner_oligotype_taxon').hide();
        $('#spinner_oligotype_taxon2').hide();
        // Clear oracle
        $("#relationTaxonByOligosaccharideType").val("");
        $('#filter_oligotype_taxon').attr('disabled', 'disabled');
        // Change URL
        window.location.replace(window.location.pathname);
      }
  });
}
// URL: oligotype
if ( taxon !== null ) {
  $('input.column_filter').val(taxon);
}

// Filter - taxon
function filterColumntaxon(i) {
  $('#results_oligotype_taxon').DataTable().column(i).search(
    $('#taxon_lt').val().replace(/;/g, "|"), true, false
  ).draw();
  checkURL();
}
$('input.column_filter').on( 'keyup click', function () {
  filterColumntaxon($(this).parents('tr').attr('data-column'));
} );

// Check url
function checkURL() {
  var url = window.location.pathname;
  if ( $("#search_oligotype option:selected").text() !== '' ) {
    url += "?oligotype=" + $("#search_oligotype option:selected").text();
  }
  if ( $("#taxon_lt").val() !== '' ) {
    url += "&taxon=" + $("#taxon_lt").val();
  }
  history.pushState({}, null, url);
}
