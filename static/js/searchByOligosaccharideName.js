taxon_lt/**
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
**/

import { format_docs, createTableRev, createTableWithArgRev } from './utils.js';

$("#searchByOligosaccharideName.nav-item").addClass( "active" );

// Spinner
$('#spinner_oligoname_taxon').show();
$('#spinner_oligoname_taxon2').show();

var thtable = $('#results_oligoname_taxon').DataTable();

var $select = $('#search_oligoname').selectize({
    valueField: 'path',
    labelField: 'name',
    searchField: 'name',
    sortField: 'name',
    placeholder: 'e.g. Isoglobotriose',
    openOnFocus: false,
    create: false,
    maxItems: 1,
    onInitialize: function() {
      var that = this;

      $.getJSON($SCRIPT_ROOT + '/_get_list_obt_class',
        { table: "list_oligo_name_taxon"},
        function(data) {
          data.forEach(function(item) {
            that.addOption(item);
          });
          $('#spinner_oligoname_taxon').hide();
          $('#spinner_oligoname_taxon2').hide();
          $('#search_oligoname option:selected').prop('disabled', false);
        });
    },
    onChange:function(value){
      if (value != '') {

        $('#filter_oligoname_taxon').removeAttr('disabled');
        $('#spinner_oligoname_taxon').show();
        $('#spinner_oligoname_taxon2').show();

        createTableRev(value, 'have_produced_tmp_1', 'results_oligoname_taxon');

        if ( $('#taxon_lt').val() != '' ) { filterColumntaxon(3); }

        $('#spinner_oligoname_taxon').hide();
        $('#spinner_oligoname_taxon2').hide();

        checkURL();
      }
      else {
            alert("No result for " + taxon);
            // Spinner off
            $('#spinner_oligoname_taxon').hide();
            $('#spinner_oligoname_taxon2').hide();
            // Clear oracle
            $("#relationTaxonByOligosaccharideName").val("");
            $('#filter_oligoname_taxon').attr('disabled', 'disabled');
            // Change URL
            window.location.replace(window.location.pathname);
          }
		}
  });

var selectize = $select[0].selectize;

// URL: taxon
if ( oligoname !== null ) {
  $('#spinner_oligoname_taxon').show();
  $('#spinner_oligoname_taxon2').show();
  selectize.setTextboxValue(oligoname);
  $("#search_oligoname option:selected").text(oligoname);
  $("#search_oligoname option:selected").val(oligoname);
  $('#filter_oligoname_taxon').removeAttr('disabled');

  $.getJSON($SCRIPT_ROOT + '/_get_path',
    {name: taxon, table: 'list_oligo_name_taxon'},
    function success(path) {
      if ( path != '' ) {
        let l_path = path[0].split('/');
        let oligonameid = l_path[l_path.length-1];

        createTableWithArgRev(oligonameid, 'have_produced_2', 'results_oligoname_taxon');

        if ( $('#taxon_lt').val() != '' ) { filterColumntaxon(3); }

        $('#spinner_oligoname_taxon').hide();
        $('#spinner_oligoname_taxon2').hide();

        checkURL();
      }
      else {
        alert("No result for " + taxon);
        // Spinner off
        $('#spinner_oligoname_taxon').hide();
        $('#spinner_oligoname_taxon2').hide();
        // Clear oracle
        $("#relationTaxonByOligosaccharideName").val("");
        $('#filter_oligoname_taxon').attr('disabled', 'disabled');
        // Change URL
        window.location.replace(window.location.pathname);
      }
  });
}
// URL: oligoname
if ( taxon !== null ) {
  $('input.column_filter').val(taxon);
}

// Filter - taxon
function filterColumntaxon(i) {
  $('#results_oligoname_taxon').DataTable().column(i).search(
    $('#taxon_lt').val().replace(/;/g, "|"), true, false
  ).draw();
  checkURL();
}
$('input.column_filter').on( 'keyup click', function () {
  filterColumntaxon($(this).parents('tr').attr('data-column'));
} );

// Check url
function checkURL() {
  var url = window.location.pathname;
  if ( $("#search_oligoname option:selected").text() !== '' ) {
    url += "?oligoname=" + $("#search_oligoname option:selected").text();
  }
  if ( $("#taxon_lt").val() !== '' ) {
    url += "&taxon=" + $("#taxon_lt").val();
  }
  history.pushState({}, null, url);
}
